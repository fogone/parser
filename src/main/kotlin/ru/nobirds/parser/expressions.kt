package ru.nobirds.parser

data class ConstantExpression<T>(val value: T) : Expression<T> {

    override fun evaluate(context: EvaluationContext<T>): T = value

}

data class UnaryOperationExpression<T>(val subject: Expression<T>, val operator: String) : Expression<T> {

    override fun evaluate(context: EvaluationContext<T>): T {
        return context.resolveUnaryOperator(operator).operate(subject.evaluate(context))
    }

}

data class BiOperationExpression<T>(val left: Expression<T>, val right: Expression<T>,
                                    val operator: String) : Expression<T> {

    override fun evaluate(context: EvaluationContext<T>): T {
        val leftValue = left.evaluate(context)
        val rightValue = right.evaluate(context)

        return context.resolveBiOperator(operator).operate(leftValue, rightValue)
    }

}

data class GroupExpression<T>(val expression: Expression<T>) : Expression<T> {

    override fun evaluate(context: EvaluationContext<T>): T = expression.evaluate(context)

}


package ru.nobirds.parser

data class NumberToken(val value:String) : Token

data class OperatorToken(val operator: String) : Token

data class SpaceToken(val count: Int = 1) : Token {
    override val ignorable: Boolean = true
}

object StartGroupToken : Token

object EndGroupToken : Token

class NumberConstantTokenBuilder : TokenBuilder {

    private val token = StringBuffer()

    override fun isApplicable(char: Char): Boolean = char.isDigit()

    override fun apply(char: Char) {
        token.append(char)
    }

    override fun toToken(): Token = NumberToken(token.toString())

    override fun toString(): String = token.toString()

}

class OperatorTokenBuilder(vararg val operators: Char) : TokenBuilder {

    private var operator: Char? = null

    override fun isApplicable(char: Char): Boolean = operator == null && char in operators

    override fun apply(char: Char) {
        operator = char
    }

    override fun toToken(): Token = OperatorToken(operator?.toString() ?: throw IllegalStateException())

    override fun toString(): String = operator.toString()
}

abstract class GroupTokenBuilder(val symbol: Char) : TokenBuilder {
    private var done = false
    override fun isApplicable(char: Char): Boolean = !done && char == symbol
    override fun apply(char: Char) {
        done = true
    }

    override fun toString(): String = symbol.toString()
}

class StartGroupTokenBuilder : GroupTokenBuilder('(') {
    override fun toToken(): Token = StartGroupToken
}

class EndGroupTokenBuilder : GroupTokenBuilder(')') {
    override fun toToken(): Token = EndGroupToken
}

class SpaceTokenBuilder : TokenBuilder {
    private var count = 0

    override fun isApplicable(char: Char): Boolean = char == ' '
    override fun apply(char: Char) {
        count++
    }

    override fun toToken(): Token = SpaceToken(count)

    override fun toString(): String = " "
}

package ru.nobirds.parser

import java.io.Reader

interface ParseContext<T> {

    fun parseNextExpression(): Expression<T>?

}

interface TokenParser<T> {

    fun parse(token: Token, left: Expression<T>?, context: ParseContext<T>): Expression<T>?

}

class Parser<T>(val tokenParser: TokenParser<T>, val bucket: () -> TokenBucket) {

    fun parse(source: Reader): Expression<T>? {
        return SimpleParseContext(tokenSequence(source, bucket()), tokenParser).parseNextExpression()
    }

    private class SimpleParseContext<T>(tokenSequence: Sequence<Token>, val tokenParser: TokenParser<T>) : ParseContext<T> {

        private val iterator = tokenSequence.filter { !it.ignorable }.iterator()

        override fun parseNextExpression(): Expression<T>? {
            var last:Expression<T>? = null

            return generateSequence {
                if (iterator.hasNext())
                    tokenParser.parse(iterator.next(), last, this).also { last = it }
                else null
            }.lastOrNull()
        }
    }

}


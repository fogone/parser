package ru.nobirds.parser

interface Expression<T> {

    fun evaluate(context: EvaluationContext<T>): T

}


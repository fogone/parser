package ru.nobirds.parser.simple

import ru.nobirds.parser.BiOperationExpression
import ru.nobirds.parser.ConstantExpression
import ru.nobirds.parser.EndGroupToken
import ru.nobirds.parser.EndGroupTokenBuilder
import ru.nobirds.parser.EvaluationContextRegistry
import ru.nobirds.parser.Expression
import ru.nobirds.parser.GroupExpression
import ru.nobirds.parser.NumberConstantTokenBuilder
import ru.nobirds.parser.NumberToken
import ru.nobirds.parser.OperatorToken
import ru.nobirds.parser.OperatorTokenBuilder
import ru.nobirds.parser.ParseContext
import ru.nobirds.parser.Parser
import ru.nobirds.parser.SpaceTokenBuilder
import ru.nobirds.parser.StartGroupToken
import ru.nobirds.parser.StartGroupTokenBuilder
import ru.nobirds.parser.Token
import ru.nobirds.parser.TokenBucket
import ru.nobirds.parser.TokenParser
import ru.nobirds.parser.UnaryOperationExpression
import ru.nobirds.parser.registerBiOperator
import ru.nobirds.parser.registerUnaryOperator
import java.math.BigDecimal

fun createSimpleTokenBucket(): TokenBucket = TokenBucket(
        ::StartGroupTokenBuilder,
        ::EndGroupTokenBuilder,
        { OperatorTokenBuilder('-', '+', '*', '/') },
        ::NumberConstantTokenBuilder,
        ::SpaceTokenBuilder
)

fun simpleParser(): Parser<BigDecimal> = Parser(SimpleTokenParser(), ::createSimpleTokenBucket)

class SimpleTokenParser : TokenParser<BigDecimal> {

    override fun parse(token: Token, left: Expression<BigDecimal>?, context: ParseContext<BigDecimal>): Expression<BigDecimal>? {
        return when (token) {
            is NumberToken -> ConstantExpression(BigDecimal(token.value))
            is StartGroupToken -> GroupExpression(context.parseNextExpression() ?: throw IllegalStateException())
            is EndGroupToken -> null
            is OperatorToken -> {
                val right = context.parseNextExpression()
                if (right != null) {
                    if (left != null) {
                        BiOperationExpression(left, right, token.operator)
                    } else {
                        UnaryOperationExpression(right, token.operator)
                    }
                } else throw IllegalArgumentException()
            }
            else -> throw IllegalStateException()
        }
    }

}

fun createDefaultSimpleEvaluationContext(): EvaluationContextRegistry<BigDecimal> = EvaluationContextRegistry<BigDecimal>()
        .apply {
            registerUnaryOperator("+", BigDecimal::plus)
            registerUnaryOperator("-", BigDecimal::unaryMinus)

            registerBiOperator("+", BigDecimal::plus)
            registerBiOperator("-", BigDecimal::minus)
            registerBiOperator("*", BigDecimal::times)
            registerBiOperator("/", BigDecimal::div)
        }


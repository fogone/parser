package ru.nobirds.parser

interface EvaluationContext<T> {

    fun resolveBiOperator(name:String):BiOperator<T>

    fun resolveUnaryOperator(name:String):UnaryOperator<T>

}

class EmptyEvaluationContext<T> : EvaluationContext<T> {
    override fun resolveBiOperator(name: String): BiOperator<T> {
        throw IllegalArgumentException()
    }

    override fun resolveUnaryOperator(name: String): UnaryOperator<T> {
        throw IllegalArgumentException()
    }
}

class EvaluationContextRegistry<T> : EvaluationContext<T> {

    private val biOperators = hashMapOf<String, BiOperator<T>>()
    private val unaryOperators = hashMapOf<String, UnaryOperator<T>>()

    fun registerBiOperator(name: String, operator: BiOperator<T>): EvaluationContextRegistry<T> = apply {
        biOperators[name] = operator
    }

    fun registerUnaryOperator(name: String, operator: UnaryOperator<T>): EvaluationContextRegistry<T> = apply {
        unaryOperators[name] = operator
    }

    override fun resolveBiOperator(name: String): BiOperator<T> {
        return biOperators[name] ?: throw IllegalArgumentException()
    }

    override fun resolveUnaryOperator(name: String): UnaryOperator<T> {
        return unaryOperators[name] ?: throw IllegalArgumentException()
    }

}

fun <T> EvaluationContextRegistry<T>.registerBiOperator(name: String, operator: (T, T)->T)
        = registerBiOperator(name, BiOperator.create(name, operator))

fun <T> EvaluationContextRegistry<T>.registerUnaryOperator(name: String, operator: (T)->T)
        = registerUnaryOperator(name, UnaryOperator.create(name, operator))

interface BiOperator<T> {

    fun operate(left: T, right: T): T

    companion object {
        fun <T> create(char: String, op: T.(T) -> T): BiOperator<T> = object : BiOperator<T> {
            override fun operate(left: T, right: T): T = left.op(right)
            override fun toString(): String = char
        }
    }
}

interface UnaryOperator<T> {

    fun operate(right: T): T

    companion object {
        fun <T> create(char: String, op: T.() -> T): UnaryOperator<T> = object : UnaryOperator<T> {
            override fun operate(right: T): T = right.op()
            override fun toString(): String = char
        }
    }

}
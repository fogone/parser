package ru.nobirds.parser

import java.io.Reader

data class CurrentAndNext(val current: Char, val next: Char?)

fun Reader.readChar(): Char? = read().takeIf { it > -1 }?.toChar()

fun Reader.asPeekableSequence(): Sequence<CurrentAndNext> {
    val first = readChar() ?: throw IllegalStateException()
    
    return generateSequence(CurrentAndNext(first, readChar())) { (_, next) ->
        if (next != null) CurrentAndNext(next, readChar())
        else null
    }
}

class TokenBucket(vararg val tokenFactories:()-> TokenBuilder) {

    private var tokenBuilders = createBucket()

    private fun createBucket() = tokenFactories.map { it() }

    fun apply(char: CurrentAndNext): Token? {
        tokenBuilders = tokenBuilders.filter { it.isApplicable(char.current) }

        require(tokenBuilders.isNotEmpty())

        tokenBuilders.forEach { it.apply(char.current) }

        return if (char.next == null || !tokenBuilders.any { it.isApplicable(char.next) }) {
            require(tokenBuilders.size == 1)

            tokenBuilders.first().toToken().also {
                tokenBuilders = createBucket()
            }
        } else null
    }
}

fun tokenSequence(reader: Reader, bucket: TokenBucket): Sequence<Token> =
        reader.asPeekableSequence().map { bucket.apply(it) }.filterNotNull()
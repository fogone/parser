package ru.nobirds.parser

interface Token {

    val ignorable: Boolean
        get() = false

}

interface TokenBuilder {

    fun isApplicable(char: Char): Boolean

    fun apply(char: Char)

    fun toToken(): Token

}

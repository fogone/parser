package ru.nobirds.parser

import ru.nobirds.parser.simple.createDefaultSimpleEvaluationContext
import ru.nobirds.parser.simple.simpleParser
import java.math.BigDecimal

fun calc(expression: String): BigDecimal {
    return simpleParser()
            .parse(expression.reader())
            ?.evaluate(createDefaultSimpleEvaluationContext()) ?: throw IllegalArgumentException()
}

fun main(args: Array<String>) {
    require(calc("(5 + -14) - 23") == BigDecimal((5 + (-14)) - 23))
    require(calc("((5 + -14) - 23) + (-10 * 6)") == BigDecimal(((5 + -14) - 23) + (-10 * 6)))
}

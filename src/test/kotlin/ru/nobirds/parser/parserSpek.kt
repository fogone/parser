package ru.nobirds.parser

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import ru.nobirds.parser.simple.SimpleTokenParser
import ru.nobirds.parser.simple.createSimpleTokenBucket
import java.math.BigDecimal

class ParserSpek : Spek({

    describe("parser") {

        val parser = Parser(SimpleTokenParser(), ::createSimpleTokenBucket)

        on("parse") {

            val expression = parser.parse("(-1 + 5)".reader())

            println(expression)

            it("should return full ast") {
                assertThat(expression, equalTo(
                        GroupExpression(
                                BiOperationExpression(
                                        GroupExpression(UnaryOperationExpression(ConstantExpression(BigDecimal("1")), "-")),
                                        ConstantExpression(BigDecimal("5")),
                                        "+"
                                )
                        ) as Expression<BigDecimal>
                ))
            }

        }

    }


})
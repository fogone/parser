package ru.nobirds.parser

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import ru.nobirds.parser.simple.createSimpleTokenBucket

class TokenSequenceSpek : Spek({

    describe("token sequence") {


        on("iterate") {
            val expression = "((5 + -14) - 23) + (-10 * 6)"

            val tokenSequence = tokenSequence(expression.reader(), createSimpleTokenBucket())

            val tokens = tokenSequence.toList()

            it("should fetch all tokens sequentially") {
                assertThat(
                        tokens,
                        contains(
                                StartGroupToken, StartGroupToken,
                                NumberToken("5"), SpaceToken(),
                                OperatorToken("+"), SpaceToken(),
                                OperatorToken("-"), NumberToken("14"),
                                EndGroupToken, SpaceToken(),
                                OperatorToken("-"), SpaceToken(),
                                NumberToken("23"), EndGroupToken,
                                SpaceToken(), OperatorToken("+"),
                                SpaceToken(), StartGroupToken,
                                OperatorToken("-"), NumberToken("10"),
                                SpaceToken(), OperatorToken("*"),
                                SpaceToken(), NumberToken("6"),
                                EndGroupToken
                        )
                )
            }


        }
    }

    describe("TokenBucket") {

        val builder1Factory = {
            object : TokenBuilder {
                private val token = StringBuilder()

                override fun isApplicable(char: Char): Boolean = char.isLetter()

                override fun apply(char: Char) {
                    token.append(char)
                }

                override fun toToken(): Token = NumberToken(token.toString())
            }
        }

        val builder2Factory = {
            object : TokenBuilder {
                private val token = StringBuilder()

                override fun isApplicable(char: Char): Boolean = char.isLetter() || char.isDigit()

                override fun apply(char: Char) {
                    token.append(char)
                }

                override fun toToken(): Token = NumberToken(token.toString())
            }
        }

        val bucket = TokenBucket(builder1Factory, builder2Factory)

        on("apply") {

            it("should not return token if multiple builds applicable to current char") {
                val token = bucket.apply(CurrentAndNext('a', '4'))
                assertThat(token, nullValue())
            }

            it("should return token if only one builder application to current char and no one to next") {
                val token = bucket.apply(CurrentAndNext('4', '-'))
                assertThat(token, equalTo(NumberToken("a4") as Token))
            }

        }

    }
})
